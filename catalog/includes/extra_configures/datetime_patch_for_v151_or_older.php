<?php
/*
 * Get time zone info from PHP config
*/
if (version_compare(PHP_VERSION, 5.3, '>='))
{
  @date_default_timezone_set(date_default_timezone_get());
}